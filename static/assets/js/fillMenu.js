var actions=new Array();
actions.push({
    name:"Inventura",
    image:{
        name: "icon3.png",
        alt: "Otvori inventuru"
    },
    link:"#inventura"
});
actions.push({
    name:"Inventar",
    image:{
        name: "icon4.svg",
        alt: "Otvori inventar"
    },
    link: "#inventar"
});
actions.push({
    name:"Prostorije",
    image:{
        name: "icon5.png",
        alt: "Otvori prostorije"
    },
    link: "#prostorije"
});
actions.push({
    name:"Zaposlenici",
    image:{
        name: "icon6.svg",
        alt: "Otvori zaposlenike"
    },
    link: "#zaposlenici"
});
actions.push({
    name:"Administracija",
    image:{
        name: "icon7.svg",
        alt: "Otvori administraciju"
    },
    link: "#admnistracija"
});

function fillMenu()
{
    for(var i in actions)
    {
        var li =document.createElement("li");
        var img=document.createElement("img");
        var a=document.createElement("a");
        var textNode=document.createTextNode(" "+actions[i].name);

        img.setAttribute("src","assets/images/icons/"+actions[i].image.name);
        img.setAttribute("alt",actions[i].image.alt);
        li.setAttribute("class","element");
        a.setAttribute("href",actions[i].link);
        a.setAttribute("class","stup1")
        li.appendChild(img);
        li.appendChild(a);
        a.appendChild(textNode);
        document.getElementById("izbornik").appendChild(li);

            if(actions[i].name=="Inventura")
            {
                li.setAttribute("id","opcije-oznaceno");
            }
    }
}

window.addEventListener("load",fillMenu)