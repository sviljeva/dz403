'use strict'

var express = require('express');
var app = express();

app.get('/', function(req, res){
    res.send("Hello from server");
});

app.use('/static',express.static('static'));
app.use('/rooms',express.static('rooms'));

app.listen(3000, function(){
    console.log("listening on port 3000 ...");
});
